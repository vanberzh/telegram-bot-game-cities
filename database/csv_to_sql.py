import sqlite3
import pandas as pd


#Читаем файл CSV и сохраняем в df
df = pd.read_csv("data.csv")

#Записываем данные из df в БД
df.to_sql(
	"cities", sqlite3.connect("database/data.sqlite3"),
	if_exists='append', index=False)

