from create_bot import db
from services.last_letter import LastLetter
from services.response import ResponseLogic


class SkipTurn:
    """
    Отвечает вместо пользователя
    """

    def __init__(self, user_id):
        self.user_id = user_id

    def skip(self):
        """
        Если есть названия на любую из букв предыдущего города,
        то пользователь как бы пропускает ход и у него отниммается 2 очка
        """

        db.get_cursor()
        if db.exist("get_base", "user_id", self.user_id):
            letter = LastLetter(self.user_id)
            if letter.verify_letter():
                db.update(-2, self.user_id)
                response = ResponseLogic(self.user_id)
                return response.get_answer()
            else:
                anwer_message = (
                    "Можете назвать любой город, так как на буквы "\
                    "предыдущего города, закончились названия в базе!!!")
                return anwer_message
        else:
            return "Ну хоть один город назовите!)))"

