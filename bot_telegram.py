from aiogram.utils import executor

from create_bot import dp
from handlers import user_commands


user_commands.register_handlers_user_commands(dp)


executor.start_polling(dp, skip_updates=True)
