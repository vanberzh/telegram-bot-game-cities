from create_bot import db
from services.last_letter import LastLetter


class ResponseLogic:
    """
    Подготовка ответа Бота
    """

    def __init__(self, user_id):
        self.user_id = user_id

    def get_answer(self):
        """
        По последней букве находит город в базе, записывает в игровую таблиц
        """
        
        db.get_cursor()
        letters = LastLetter(self.user_id)
        last_letter = letters.verify_letter()
        city = db.search_city(last_letter, self.user_id)
        db.insert("get_base", (self.user_id, city[0]))
        last_letter = letters.verify_letter()
        return f"🔸  {city[1]}  🔸\n\nСтрана: {city[2]}\nВам на '{last_letter}'"

