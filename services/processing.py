from create_bot import db
from services.last_letter import LastLetter
from services.response import ResponseLogic


class Processing:
    """
    Обработка ответа пользователя
    """

    def __init__(self, city_from_user, user_id):
        self.city_from_user = city_from_user
        self.user_id = user_id


    def reply_user(self):
        """
        Проверяет указанный город на возможность использования.
        Записывает его в игровую таблицу и повышает счёт пользователя только
        если этот город есть в базе и не был задействован ранее,
        в противном случае просит ввести заново.
        """

        db.get_cursor()
        letters = LastLetter(self.user_id)

        if db.exist("rating", "user_id", self.user_id):
            pass
        else:
            return "❗️Нажмите в меню 'Начать игру'❗️"
        
        if letters.verify_letter() is not None:
            if letters.verify_letter() != self.city_from_user[0].upper():
                letter = letters.verify_letter()
                return f"Не на ту букву, нужно на {letter}!"
        if self.city_from_user[0].isupper():
            pass
        else:
            return "Попробуйте написать с заглавной буквы!)"

        city = db.verify_city(self.city_from_user, self.user_id)

        if city:
            db.insert("get_base", (self.user_id, city[0]))
            db.update(1, self.user_id)
            
            if letters.verify_letter():
                response = ResponseLogic(self.user_id)
                return response.get_answer()
            else:
                anwer_message = (
                    "Можете назвать любой город, так как на буквы "\
                    "предыдущего города, закончились названия в базе!!!")
                return anwer_message
                
        elif db.exist("cities", "city", self.city_from_user):
            return "Такой город уже был! Попробуйте другой!"
        else:
            return "Такого города нет в базе! Попробуйте ещё!"

