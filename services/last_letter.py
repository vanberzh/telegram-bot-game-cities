from create_bot import db


class LastLetter:
    """
    Определяет последнюю букву для запроса
    """

    def __init__(self, user_id):
        self.user_id = user_id

    def verify_letter(self):
        """
        Ищет букву, на которую можно назвать город
        """
        db.get_cursor()
        city_id = db.over_city(self.user_id)
        if city_id:
            letters = db.exist("cities", "city_id", city_id[0])
            for elem in list(reversed(letters[1])):
                if db.search_city(elem.upper(), self.user_id):
                    letter = elem.upper()[::]
                    return letter
                    break

