import sqlite3


class DataBase:
    """
    Работа с БД
    """


    def __init__(self, base_name):
        """
        Инициализация соединения с БД
        """

        self.conn = sqlite3.connect(base_name)
        self.cursor = self.conn.cursor()


    def insert(self, table, values):
        """
        Заполняет данными таблицу
        """

        placeholders = ", ".join("?" * len(values))
        self.cursor.execute(
            f"""INSERT INTO {table}
                VALUES ({placeholders})""", values)
        return self.conn.commit()


    def update(self, value, user_id):
        """
        Обновляет колличество очков в таблице
        """

        self.cursor.execute(
            f"""UPDATE rating
                SET score=score+{value}
                WHERE user_id=(?)""", (user_id, ))
        self.conn.commit()


    def delete(self, table, user_id):
        """
        Удаляет данные из таблицы
        """

        self.cursor.execute(
            f"""DELETE FROM {table}
                WHERE user_id=(?)""", (user_id, ))
        self.conn.commit()


    def exist(self, table, column, value):
        """
        Возвращает True, если такая запись есть в БД
        """

        self.cursor.execute(
            f"""SELECT *
                FROM {table}
                WHERE {column}='{value}'""")
        return self.cursor.fetchone()


    def verify_city(self, value, user_id):
        """
        Возвращает город в виде списка (id, город),
        если он не был назван ранее
        """

        self.cursor.execute(
            f"""SELECT c.city_id, c.city
                FROM cities as c
                LEFT JOIN get_base as g
                ON c.city_id=g.city_id
                AND g.user_id=(?)
                WHERE g.city_id is null
                AND c.city='{value}'""", (user_id, ))
        return self.cursor.fetchone()


    def search_city(self, letter, user_id):
        """
        Возвращает не названный ранее город на указанную букву
        в виде списка (id, город, страна)
        """

        self.cursor.execute(
            f"""SELECT c.city_id, c.city, c.country
                FROM cities as c
                LEFT JOIN get_base as g
                ON c.city_id=g.city_id
                AND g.user_id=(?)
                WHERE g.city_id is null
                AND c.city LIKE '{letter}%'
                ORDER BY RANDOM()
                LIMIT 1""", (user_id, ))
        return self.cursor.fetchone()


    def over_city(self, user_id):
        """
        Находит последний названный город в игре
        """

        self.cursor.execute(
            f"""SELECT city_id
                FROM get_base
                WHERE user_id=(?)
                ORDER BY _rowid_ DESC
                LIMIT 1""", (user_id, ))
        return self.cursor.fetchone()


    def get_score(self, user_id):
        """
        Показывает счёт по id пользователя
        """

        self.cursor.execute(
            f"""SELECT score
                FROM rating
                WHERE user_id=(?)""", (user_id, ))
        return self.cursor.fetchone()[0]


    def get_rating(self, user_id):
        """
        Возвращает место пользователя в рейтинге
        """

        self.cursor.execute(
            f"""SELECT COUNT()
                FROM rating
                WHERE score >= (SELECT DISTINCT score
                                FROM rating
                                WHERE user_id=(?))""", (user_id, ))
        return self.cursor.fetchone()[0]


    def create_tab(self):
        """
        Создаёт БД необходимыми таблицами
        """

        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS cities (
            city_id INTEGER, city TEXT, country TEXT)""")

        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS get_base (
                user_id INTEGER, city_id INTEGER)""")

        self.cursor.execute(
            """CREATE TABLE IF NOT EXISTS rating (
                user_id INTEGER, score INTEGER)""")
        
        self.conn.commit()


    def get_cursor(self):
        """
        Возвращает курсор
        """

        self.cursor = self.conn.cursor()

