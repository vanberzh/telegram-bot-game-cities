from aiogram import types

from create_bot import dp
from create_bot import Dispatcher
from services.processing import Processing
from services.skip_turn import SkipTurn
from services.user import UserData


async def send_menu(message: types.Message):
	await message.answer(
		"ℹ️  Мои команды:\n\n"
        "🔸Начать игру\n"
        "🔸Помощь\n"
        "🔸Пропустить ход (-2 балла)\n"
        "🔸Посмотреть счёт и рейтинг\n"
        "🔸Начать игру заново (счёт обнулится)\n"
        "🔸Удалиться из игры\n\n"
        "ℹ️  За правильно названный город, вам начисляется 1 балл.")


async def start(message: types.Message):
	obj = UserData(message.from_user.id)
	obj.add_user()
	await message.answer(
		"Привет! Я бот для игры в 'Города'!\n"
        "Назовите любой город!")
	await send_menu(message=message)


async def reset(message: types.Message):
	obj = UserData(message.from_user.id)
	answer_message = obj.reset_result()
	await message.answer(answer_message)


async def delete(message: types.Message):
	obj = UserData(message.from_user.id)
	answer_message = obj.del_user()
	await message.answer(answer_message)


async def score(message: types.Message):
	obj = UserData(message.from_user.id)
	answer_message = obj.score_user()
	await message.answer(answer_message)
    

async def skip(message: types.Message):
	obj = SkipTurn(message.from_user.id)
	answer_message = obj.skip()
	await message.answer(answer_message)


async def request_to_db(message: types.Message):
	text = message.text
	if text and not text.startswith('/'):
		answer = Processing(message.text, message.from_user.id)
		answer_message = answer.reply_user()
		await message.answer(answer_message)


#Регистрация хендлеров
def register_handlers_user_commands(dp : Dispatcher):
	dp.register_message_handler(send_menu, commands=['help'])
	dp.register_message_handler(start, commands=['start'])
	dp.register_message_handler(reset, commands=['reset'])
	dp.register_message_handler(delete, commands=['delete'])
	dp.register_message_handler(score, commands=['score'])
	dp.register_message_handler(skip, commands=['skip'])
	dp.register_message_handler(
		request_to_db, content_types=types.ContentTypes.TEXT)