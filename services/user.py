from create_bot import db


class UserData:
    """
    Работа с данными пользователя
    """

    def __init__(self, user_id):
        self.user_id = user_id

    def add_user(self):
        """
        Добавление пользователя в БД
        """

        db.get_cursor()
        if db.exist("rating", "user_id", self.user_id):
            pass
        else:
            db.insert("rating", (self.user_id, 0))

    def del_user(self):
        """
        Удаление пользователя из БД
        """

        db.get_cursor()
        if db.exist("rating", "user_id", self.user_id):
            db.delete("rating", self.user_id)
            db.delete("get_base", self.user_id)
            return "Ваши данные удалены из игры!"

    def score_user(self):
        """
        Показывает очки и рейтинг пользователя
        """

        db.get_cursor()
        if db.exist("rating", "user_id", self.user_id):
            score = db.get_score(self.user_id)
            rating = db.get_rating(self.user_id)
            return (
                f"Колличество набранных вами баллов равно {score},\n"
                f"Занимаете {rating} место в рейтинге!")
        else:
            return "Вас нет в игре!\nДля начала игры нажмите /start"

    def reset_result(self):
        """
        Сбрасывает результаты игры
        """

        db.get_cursor()
        db.delete("rating", self.user_id)
        db.delete("get_base", self.user_id)
        db.insert("rating", (self.user_id, 0))
        return "Ваши результаты игры обнулены!"

