import logging
import os

from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from dotenv import load_dotenv
from pathlib import Path

from database.db import DataBase


logging.basicConfig(
	format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
	level=logging.INFO
	)
logger = logging.getLogger(__name__)


load_dotenv()
env_path = Path('.')/'.env'
load_dotenv(dotenv_path=env_path)


storage = MemoryStorage()
bot = Bot(token=os.getenv("TOKEN"))
dp = Dispatcher(bot, storage=storage)


DATABASE_NAME  = "database/data.sqlite3"
db = DataBase(DATABASE_NAME)
db.create_tab()

